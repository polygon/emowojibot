# emowoji-bot
A smol (somewhat shitty) matrix bot meant to add a huuge amount of custom emotes to a room.
Usage: 
* start like ./emowojí_bot <homeserver-url> <username> <password>
* invite the bot to a room
* type `!add <desired_image_pack_name>`
* let it to its thing

The maximum size of a state event seems to be around 64kB, according to a person from a matrix chat,
to stay fairly beyond this limitation, the bot will keep track of the estimated payload size and if it exceeds 50kb, send the image pack event and start building the next one, resulting in *a lot* of small image packs.