use image::io::Reader as ImageReader;
use matrix_sdk::{
    self,
    config::SyncSettings,
    ruma::{
        events::room::{
            member::StrippedRoomMemberEvent,
            message::{MessageType, OriginalSyncRoomMessageEvent},
        },
        RoomId,
    },
    Client, Room, RoomState,
};
use mime_guess;
use mime_guess::from_path;
use serde::{Deserialize, Serialize};
use serde_json;
use serde_json::{json, Value as JsonValue};
use std::{collections::HashMap, env, fs, path::Path, process::exit};
use tokio::time::{sleep, Duration};
use tracing::{error, info};
use tracing_subscriber;
use uuid::Uuid;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    let (homeserver_url, username, password) =
        match (env::args().nth(1), env::args().nth(2), env::args().nth(3)) {
            (Some(a), Some(b), Some(c)) => (a, b, c),
            _ => {
                error!(
                    "Usage: {} <homeserver_url> <username> <password>",
                    env::args().next().unwrap()
                );
                exit(1);
            }
        };
    login_and_sync(homeserver_url, &username, &password).await?;
    Ok(())
}

async fn login_and_sync(
    homeserver_url: String,
    username: &str,
    password: &str,
) -> anyhow::Result<()> {
    let client = Client::builder()
        .homeserver_url(homeserver_url)
        .build()
        .await?;

    client
        .matrix_auth()
        .login_username(username, password)
        .initial_device_display_name("Emoji Mass Uploader")
        .send()
        .await?;

    info!("Logged in as {}", username);

    client.add_event_handler(on_stripped_state_member);
    let sync_token = client.sync_once(SyncSettings::default()).await?.next_batch;
    client.add_event_handler(on_room_message);
    let settings = SyncSettings::default().token(sync_token);
    client.sync(settings).await?;
    Ok(())
}

async fn on_stripped_state_member(
    room_member: StrippedRoomMemberEvent,
    client: Client,
    room: Room,
) {
    if room_member.state_key != client.user_id().unwrap() {
        return;
    }

    tokio::spawn(async move {
        info!("Autojoining room {}", room.room_id());
        let mut delay = 2;

        while let Err(err) = room.join().await {
            error!(
                "Failed to join room {} ({:?}), retrying in {}s",
                room.room_id(),
                err,
                delay
            );
            sleep(Duration::from_secs(delay)).await;
            delay *= 2;

            if delay > 3600 {
                error!("Can't join room {} ({:?})", room.room_id(), err);
                break;
            }
        }
        info!("Successfully joined room {}", room.room_id());
    });
}

#[derive(Serialize, Deserialize)]
struct ImageStateEvent {
    state_key: String,
}

async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room, client: Client) {
    if room.state() != RoomState::Joined {
        return;
    }
    let MessageType::Text(text_content) = event.content.msgtype else {
        return;
    };
    let mut parts = text_content.body.trim().splitn(2, ' ');
    let command = parts.next().unwrap_or_default();

    if command == "!add" {
        let pack_name = parts.next().unwrap_or_default().to_string();

        info!("Received add command for {}", pack_name);

        if let Err(e) = process_image_pack(&client, &room, &pack_name).await {
            error!("Failed to process emoji pack: {:?}", e);
        }
    }
}

async fn upload_image(client: &Client, file_path: &str) -> anyhow::Result<String> {
    info!("Uploading image {}", file_path);
    let image_data = tokio::fs::read(file_path).await?;
    let content_type = mime_guess::from_path(file_path).first_or_octet_stream();

    let response = client.media().upload(&content_type, image_data).await?;
    info!("Finished uploading");
    Ok(response.content_uri.to_string())
}

fn create_image_object(mxc_url: String, path_string: &str) -> anyhow::Result<JsonValue> {
    info!("Creating image object for {}", &mxc_url);
    let path = Path::new(&path_string);
    let img = ImageReader::open(&path)?.decode()?;
    let h = img.height();
    let w = img.width();
    let metadata = path.metadata()?;
    let size = metadata.len();
    let mime_type = from_path(&path)
        .first_or_octet_stream()
        .essence_str()
        .to_string();

    Ok(serde_json::json!({
        "url": mxc_url,
        "info": {
            "w": w,
            "h": h,
            "size": size,
            "mimetype": mime_type,
        }
    }))
}

fn create_pack_event(images: &HashMap<String, JsonValue>, pack_name: &str) -> JsonValue {
    info!("Creating im.ponies.room_emotes event");
    serde_json::json!({
        "images": images,
        "pack": {
            "display_name": pack_name,
        }
    })
}

async fn send_pack_event(
    client: &Client,
    room_id: &RoomId,
    event: JsonValue,
) -> anyhow::Result<()> {
    info!("Sending im.ponies.room_emotes event to the room");
    if let Some(room) = client.get_room(&room_id) {
        let uuid = Uuid::new_v4();
        let response = room
            .send_state_event_raw("im.ponies.room_emotes", &uuid.to_string(), json!(event))
            .await?;
        info!("Got event id {}", response.event_id);
    }
    Ok(())
}

fn estimate_payload_size(
    images: &HashMap<String, JsonValue>,
    new_image: &JsonValue,
    shortcode: String,
) -> usize {
    let mut tmp_map = images.clone();
    tmp_map.insert(shortcode, new_image.clone());
    serde_json::to_string(&tmp_map).unwrap_or_default().len()
}

async fn process_image_pack(client: &Client, room: &Room, pack_name: &str) -> anyhow::Result<()> {
    let mut images = HashMap::new();
    let max_size = 50 * 1024;

    for entry in fs::read_dir("emotes/")? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            let mxc_url = upload_image(client, path.to_str().unwrap()).await?;
            let shortcode = path.file_stem().unwrap().to_str().unwrap().to_string();
            match create_image_object(mxc_url, path.to_str().unwrap()) {
                Ok(image_object) => {
                    let estimated_size =
                        estimate_payload_size(&images, &image_object, shortcode.clone());
                    if estimated_size >= max_size {
                        info!("estimated payload size exceeded, sending pack event..");
                        let image_pack_event = create_pack_event(&images, pack_name);
                        send_pack_event(client, room.room_id(), image_pack_event).await?;
                        images.clear();
                    }
                    //sleep(Duration::from_secs(1)).await;
                    images.insert(shortcode, image_object);
                }
                Err(e) => {
                    error!("Error processing image {}: {}", shortcode, e);
                }
            }
        }
    }
    info!("Sending pack event with remaining images");
    if !images.is_empty() {
        send_pack_event(
            client,
            room.room_id(),
            create_pack_event(&images, pack_name),
        )
        .await?;
    }
    Ok(())
}
